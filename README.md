# README

This is an attempt to create a rails 6 project from scratch using the getting started guide.

Am attempting to use emacs with some modifications, major and minor modes to accomplish the goal.

![Emacs in source](app/assets/images/2020-08-05.13-23-04.png)

Software Versions:

![Software Versions](app/assets/images/Software.Versions.png)

## Phase One:

---

Branched after initial commits to phase one. The plan is to add the first controller then the second in phase two.

![Welcome Controller](app/assets/images/Welcome.Controller.png)

Working software with new controller.
Simply hitting the controller.

![Welcome Controller working](app/assets/images/Welcome.Rails.png)

Rails routes partial list:

![Routes](app/assets/images/Rails.Routes.png)

Correct error throw from service.
Model Validation error:

![Model Validation](app/assets/images/Model.Validation.png)

Article Error Handling:

![Model Validation](app/assets/images/Article.Error.Handling.png)

Articles without the comments model or controller added.
Rails service in a working state.

![Phase One Complete](app/assets/images/Phase.One.Complete.png)

## Phase One Complete:

---

## Phase Two:

Comment Model addition and mirge.

![Comment Model Addtion](app/assets/images/Comment.Model.png)

Comment controller addition.

![Comment Model Addtion](app/assets/images/CommentController.png)

Comments in a working state.
![Comments Working](app/assets/images/CommentsWorking.png)

Authenication working in a new private browser.
![Authencation](app/assets/images/Authencation.Working.png)

Rails service in a working state.

## Phase Two Complete

---
Emacs gotchas that I ran into and would like to mention:

```script
sr-speedbar-refresh-turn-off
```
The bar wants to refresh its contents at bad spots. This kills that.
